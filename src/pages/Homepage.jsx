import React from 'react'
import fotoSendiri from '../images/FotoSendiri.jpg'
import logoBinar from '../images/logoBinar.svg'
import logoSkilvul from '../images/logoSkilvul.png'
import logoSungaiIndah from '../images/logoSungaiIndah.jpg'
import logoHimpunanElektroUntid from '../images/logoHimpunanElektroUntid.jpg'
import logoUntid from '../images/logoUntid.png'
import logoAsiaUni from "../images/logoAsiaUni.png"
import logogdsc from '../images/logogdsc.jpg'
import garisGaris from '../images/garisGaris.png'
import './Homepage.css'

import {SiLinkedin} from 'react-icons/si'
import {SiGithub} from 'react-icons/si'
import {SiGitlab} from 'react-icons/si'
import {FaReact} from 'react-icons/fa'
import {SiExpress} from 'react-icons/si'
import {SiBootstrap} from 'react-icons/si'
import {SiPostgresql} from 'react-icons/si'
import {SiMongodb} from 'react-icons/si'
import {SiJest} from 'react-icons/si'
import {SiSocketdotio} from 'react-icons/si'
import {FaGit} from 'react-icons/fa'


export default function Homepage() {
  return (
    <>

      <div className="d-flex justify-content-center mx-0 px-0 d-none d-md-block" >
          <nav id="navbar-example2" class="navbar navbar-expand-lg rounded-3 bg-light bg-opacity-75 d-flex mx-md-5 mx-3 justify-content-center fixed-top mt-4"  >
            <ul class="nav nav-pills">
              <li class="nav-item">
                <a class="nav-link" href="#scrollspyHeading1">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#scrollspyHeading2">Experiences</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#scrollspyHeading4">Educations & Organization</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#scrollspyHeading6">Honor-Publication</a>
              </li>
            </ul>
          </nav>
      </div>
        
      <div data-bs-spy="scroll" data-bs-target="#navbar-example2" data-bs-root-margin="0px 0px -40%" data-bs-smooth-scroll="true" class="scrollspy-example rounded-2 " tabindex="0">

        {/* Foto dan Hi There ! */}
        <div className="row pb-5 rounded-3 px-1 px-md-5 w-100 mx-0 padding-top-15vh" style={{ backgroundImage: `url(${garisGaris})`}} id='scrollspyHeading1' >
          <div className="col-md-6 col-12 d-flex justify-content-center align-items-center ">
            <img src={fotoSendiri} alt="" srcset="" style={{ height: "33vh" }} className='rounded-3 shadow-lg'/>
          </div>
          <div className="col-md-6 col-12 d-flex align-items-center rounded-start pe-md-5 mt-4 mt-md-0 shadow" style={{ backgroundColor: `white`}}>
            <div>
              <div className='h1 pb-2 mt-5' >Hi There !</div>
              <div className='h4 pb-1'>
                I'm Jansen Conggono 
                <span className='ps-3' onClick={() => window.open('https://linkedin.com/in/jansen-c')}><SiLinkedin/></span>
                <span className='ps-3' onClick={() => window.open('https://github.com/Jansen-c')}><SiGithub/></span>
                <span className='ps-3' onClick={() => window.open('https://gitlab.com/jansen08r')}><SiGitlab/></span>
              </div>
              <div className='fs-5 pb-5'>
                Electrical engineering major student in Tidar University, currently at 7th semester and concentrating on Information Technology. I have attended several fullstack bootcamps held by Kampus Merdeka. Looking forward to work on web development or IT related fields.
              </div>
              <div className='h6 pb-1'>
                Technologies I've been Working With
              </div>
              <div className='d-flex flex-wrap '>
                <button type="button" class="btn btn-outline-dark me-3 mb-3 d-flex align-items-center " onClick={() => window.open('https://reactjs.org/')}><FaReact/> ReactJs</button>
                <button type="button" class="btn btn-outline-dark me-3 mb-3 d-flex align-items-center " onClick={() => window.open('https://expressjs.com/')}><SiExpress/> ExpressJs</button>
                <button type="button" class="btn btn-outline-dark me-3 mb-3 d-flex align-items-center " onClick={() => window.open('https://getbootstrap.com/')}><SiBootstrap/> Bootstrap</button>
                <button type="button" class="btn btn-outline-dark me-3 mb-3 d-flex align-items-center " onClick={() => window.open('https://www.postgresql.org/')}><SiPostgresql/> PostgreSQL</button>
                <button type="button" class="btn btn-outline-dark me-3 mb-3 d-flex align-items-center " onClick={() => window.open('https://www.mongodb.com/')}><SiMongodb/> MongoDB</button>
                <button type="button" class="btn btn-outline-dark me-3 mb-3 d-flex align-items-center " onClick={() => window.open('https://jestjs.io/')}><SiJest/> Jest</button>
                <button type="button" class="btn btn-outline-dark me-3 mb-3 d-flex align-items-center " onClick={() => window.open('https://socket.io/')}><SiSocketdotio/> Socket.io</button>
                <button type="button" class="btn btn-outline-dark me-3 mb-3 d-flex align-items-center " onClick={() => window.open('https://git-scm.com/')}><FaGit/> Git</button>
                <button type="button" class="btn btn-outline-dark me-3 mb-3 d-flex align-items-center " onClick={() => window.open('https://cloudinary.com/')}>Cloudinary</button>
              </div>

            </div>
          </div>
        </div>

        {/* Exp so far */}
        <div className="row g-4 pt-5 px-1 px-md-5 mx-md-5 mx-0 " id='scrollspyHeading2'>
          <div className='fs-3'>My Experience So Far</div>
          <div className="col-md-6 col-12">
            {/* kok malah justify content center yang buat dia lurus yak ? */}
            <div class="card h-100 d-flex justify-content-center py-5">
              <div class="row">
                <div class="col-md-4 d-flex justify-content-center align-items-center">
                  <img src={logoBinar} class="img-fluid rounded-start w-50 " alt="..." />
                </div>
                <div class="col-md-8">
                  <div class="card-body">
                    <h5 class="card-title">Studi Independen Fullstack Web Development</h5>
                    <p class="mb-0"><small class="text-muted">Binar Academy</small></p>
                    <p class="card-text"><small class="text-muted">Feb 2022 - Jul 2022</small></p>
                    <p class="card-text">Working on group consisting of 6 members. I am in charge as tech lead in frontend development, responsible for making sure thaht the finished application looks similar to desired mock-up design and works according to the business flow.</p>
                    <div className='d-flex d-md-block justify-content-center'>
                      <button type="button" class="btn btn-outline-success" onClick={() =>  window.open('https://gitlab.com/jansen08r/secondhand-final-project')}>Repository</button>
                    </div>
                    {/* Button Trigger Modal */}
                    {/* <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                      Documentations
                    </button> */}

                    {/* Modal */}
                    {/* <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Documentations</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                          </div>
                          <div class="modal-body">
                            ...
                          </div>
                          
                        </div>
                      </div>
                    </div> */}
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-12">

            <div class="card h-100 d-flex justify-content-center py-5">
              <div class="row">
                <div class="col-md-4 d-flex justify-content-center align-items-center">
                  <img src={logoSkilvul} class="img-fluid rounded-start w-50 " alt="..." />
                </div>
                <div class="col-md-8">
                  <div class="card-body">
                    <h5 class="card-title">Studi Independen Junior Web Developer</h5>
                    <p class="mb-0"><small class="text-muted">Skilvul</small></p>
                    <p class="card-text"><small class="text-muted">Aug 2021 - Jan 2022</small></p>
                    <p class="card-text">Part of Skilvul #Tech4Impact implemented by Skilvul for Kampus Merdeka program held by Kemendikbud. Our team consists of 4 members and in partner with National University of Singapore (NUS) to deliver application called INCOV as a way to helping people who are self-quarantining. My main 
                    responsible is working on backend such as using JWT Token, connecting to Mongodb Atlas using Mongoose, and deployment process using Heroku</p>
                    <div className='d-flex d-md-block justify-content-center'>
                      <button type="button" class="btn btn-outline-success" onClick={() =>  window.open('https://github.com/project-akhir')}>Repository</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-12">

            <div class="card h-100 d-flex justify-content-center py-5">
              <div class="row">
                <div class="col-md-4 d-flex justify-content-center align-items-center">
                  <img src={logoHimpunanElektroUntid} class="img-fluid rounded-start w-50 " alt="..." />
                </div>
                <div class="col-md-8">
                  <div class="card-body">
                    <h5 class="card-title">Training of Trainers Microcontroller Tidar University</h5>
                    <p class="mb-0"><small class="text-muted">Himpunan Mahasiswa Teknik Elektro Universitas Tidar</small></p>
                    <p class="card-text"><small class="text-muted">Aug 2021</small></p>
                    <p class="card-text">Responsible for leading group of 3 making simple Microcontroller circuit using Proteus and Arduino IDE</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-12">
          
            <div class="card h-100 d-flex justify-content-center py-5">
              <div class="row">
                <div class="col-md-4 d-flex justify-content-center align-items-center">
                  <img src={logoSungaiIndah} class="img-fluid rounded-start w-50 " alt="..." />
                </div>
                <div class="col-md-8">
                  <div class="card-body">
                    <h5 class="card-title">Junior Electrical Engineering Intern</h5>
                    <p class="mb-0"><small class="text-muted">PT. Sungai Indah</small></p>
                    <p class="card-text"><small class="text-muted">Jun 2020 - Aug 2020</small></p>
                    <p class="card-text">Working on PLC, electrical panel and components within, wiring, and soldering electrical components.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        {/* Education & organization*/}
        <div className="row g-4 pt-5 px-1 px-md-5 mx-md-5 mx-0" id='scrollspyHeading4'>
          <div className='fs-3'>Educations & Organization</div>
          <div className="col-md-6 col-12">
            {/* kok malah justify content center yang buat dia lurus yak ? */}
            <div class="card h-100 d-flex justify-content-center py-5">
              <div class="row">
                <div class="col-md-4 d-flex justify-content-center align-items-center">
                  <img src={logoUntid} class="img-fluid rounded-start w-50 " alt="..." />
                </div>
                <div class="col-md-8">
                  <div class="card-body">
                    <h5 class="card-title">Universitas Tidar</h5>
                    <p class="mb-0"><small class="text-muted">Bachelor's degree, Electrical Engineering</small></p>
                    <p class="card-text"><small class="text-muted">2019 - 2023</small></p>
                    <p class="card-text">GPA : 3.48/4.00</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-12">

            <div class="card h-100 d-flex justify-content-center py-5">
              <div class="row">
                <div class="col-md-4 d-flex justify-content-center align-items-center">
                  <img src={logoAsiaUni} class="img-fluid rounded-start w-50 " alt="..." />
                </div>
                <div class="col-md-8">
                  <div class="card-body">
                    <h5 class="card-title">Asia University (Taiwan)</h5>
                    <p class="mb-0"><small class="text-muted">Bachelor's degree, Information Technology</small></p>
                    <p class="card-text"><small class="text-muted">Sep 2021 - Jan 2022</small></p>
                    <p class="card-text">Grade : 94/100</p>
                    <p class="card-text">Virtual Exchange Program Fall Semester 2021. Study data science courses with a total of 3 academic credits earned.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-12">

            <div class="card h-100 d-flex justify-content-center py-5">
              <div class="row">
                <div class="col-md-4 d-flex justify-content-center align-items-center">
                  <img src={logogdsc} class="img-fluid rounded-start w-75  " alt="..." />
                </div>
                <div class="col-md-8">
                  <div class="card-body">
                    <h5 class="card-title">Google Developer Students Clubs Tidar University</h5>
                    <p class="mb-0"><small class="text-muted">Member</small></p>
                    <p class="card-text"><small class="text-muted">Sep 2021 - Jul 2022</small></p>
                    <p class="card-text">Learning things related to technology owned by Google and fields related to IT such as web development, android devolopment, and machine learning</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        
        {/* Honor and Publications */}
        <div className="row pt-5 px-md-5 mx-md-5 mx-0" style={{ height: "45vh" }} id='scrollspyHeading6'>
          <div className="col-12 pb-5 px-3">
            <ul class="list-group list-group-flush">
              <li class="list-group-item">
                <div className='fs-4 pb-3'>Honor</div>
                <div className='h6'>Lolos Pendanaan Program Mahasiswa Wirausaha (PMW) 2021 Universitas Tidar</div>
              </li>
              <li class="list-group-item text-break">
                <div  className='fs-4 pb-3'>Publication</div>
                <div className='h6'>Implementasi Fuzzy Time Series Chen untuk Prediksi Jumlah Mahasiswa Baru Universitas Tidar</div>
                <div className='d-flex justify-content-center pt-2'>
                  <button type="button " style={{borderRadius:"60px"}} class="btn btn-light w-75" onClick={() => window.open('http://dx.doi.org/10.33387/protk.v9i1.3751')} >DOI (Digital Object Identifier)</button>
                </div>
              </li>
            </ul>
          </div>
        </div>
        

      </div>
      <footer class="d-flex flex-wrap justify-content-center align-items-center py-3 my-4 border-top">
          <div class=" d-flex align-items-center justify-content-center flex-column">
            <div class="text-muted pb-2">2022 © made with love - by Jansen</div>
            <div class="text-muted px-1 px-md-5 px-md-0  text-center">Please feel free to contact me at jansen08r@gmail.com or (+62)81344881956</div>
          </div>
        </footer>
      
    </>

  )
}
